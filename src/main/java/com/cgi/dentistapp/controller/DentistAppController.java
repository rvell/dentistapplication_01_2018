package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;
    
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }

    @GetMapping("/results")
    public String showResults(Model model){
        model.addAttribute("registratsioonid", dentistVisitService.listVisits());
        return "/results";
    }

    @PostMapping("/results")
    public String registrationResults(@RequestParam("dentistName") String searchWord, Model model){
        model.addAttribute("registratsioonid", dentistVisitService.listVisits());

        List<DentistVisitEntity> visits = dentistVisitService.listVisits();
        ArrayList<DentistVisitEntity> results = new ArrayList<DentistVisitEntity>();

        for(int i = 0; i < visits.size(); i++ ){
            if(visits.get(i).getDentistName().toLowerCase().contains(searchWord.toLowerCase())){
                results.add(visits.get(i));
            }
        }
        model.addAttribute("results", results);
        return "results";
    }

    @GetMapping("/")
    public String showRegisterForm(DentistVisitDTO dentistVisitDTO) {
        return "form";
    }

    @PostMapping("/")
    public String postRegisterForm(Model model, @Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult) {

        //Check for prior registrations through existsVisit method.
        boolean exists = dentistVisitService.existsVisit(dentistVisitDTO.getDentistName(),
                dentistVisitDTO.getVisitTime());
        if(exists){
            model.addAttribute("exists", dentistVisitDTO.getVisitTime());
            return "form";
        }

        //Check for working hours on weekdays (10-16 From monday to Friday). Capitalism accepts no breaks.
        if(bindingResult.hasErrors() || !dentistVisitService.areWorkingHours(dentistVisitDTO.getVisitTime().getHours(),
                dentistVisitDTO.getVisitTime().getDay())){
            if(dentistVisitDTO.getVisitTime() == null ||
                    !dentistVisitService.areWorkingHours(dentistVisitDTO.getVisitTime().getHours(),
                            dentistVisitDTO.getVisitTime().getDay())){
                model.addAttribute("error", dentistVisitDTO.getVisitTime());
            }
            return "form";
        }
        dentistVisitService.addVisit(dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitTime());
        System.out.println(dentistVisitDTO.getVisitTime());
        return "redirect:/results";
    }

    @GetMapping
    public String showDetail (@RequestParam("id") Long id, Model model, DentistVisitDTO dentistVisitDTO){
        List<DentistVisitEntity> visits = dentistVisitService.listVisits();

        for (int i = 0; i < visits.size(); i++ ){
            if(visits.get(i).getId().equals(id)){
                DentistVisitEntity dve = visits.get(i);
                dentistVisitDTO.setDentistName(dve.getDentistName());
                dentistVisitDTO.setVisitTime(dve.getVisitTime());
                model.addAttribute("result", dve);
            }
        }
        return "details";
    }

}
