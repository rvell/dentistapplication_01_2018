package com.cgi.dentistapp.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    private DentistVisitDao dentistVisitDao;

    /**
     * Creates new registrations via dentistVisitDao
     * @param dentistName
     * @param visitTime
     */
    public void addVisit(String dentistName, Date visitTime) {
        DentistVisitEntity visit = new DentistVisitEntity(dentistName, visitTime);
        dentistVisitDao.create(visit);
    }
    /**
     * Creates a list of all the visits.
     */
    public List<DentistVisitEntity> listVisits () {
        return dentistVisitDao.getAllVisits();
    }

    /**
     * Check if the registration time has already been allocated for specific dentist.
     * @param dentistName
     * @param visitTime
     * @return boolean exists
     */
    public boolean existsVisit(String dentistName, Date visitTime){

        List<DentistVisitEntity> visits = dentistVisitDao.getAllVisits();
        boolean exists = false;

        for(int i = 0; i != visits.size(); i++){
            if(dentistName.equals(visits.get(i).getDentistName())
                    && visitTime.equals(visits.get(i).getVisitTime())){
                exists = true;
            }
        }
        return exists;
    }

    /**
     * Validates that user input hours are in between 10-16 on weekdays
     * Week starts with sunday(0) ends with saturday(6)
     * @param hour
     * @param day
     * @return boolean
     */
    public boolean areWorkingHours(int hour, int day){
        if (hour > 16 || hour < 10 || day == 0 || day == 6){
            return false;
        }
        return true;
    }

}
